from turtle import *
from emoji import *

def next_player(current_player):
    # Copy your next_color function from color.py here
    if current_player == 0:
        return 1
    elif current_player == 1:
        return 0


def make_move(player, position):
    penup()
    if int(position) <= 3:
        spot = -200 + 100 * (int(position)-1)
        goto(spot,100)
        if player == 0:
            eye()
        else:
            home()
    elif int(position) <= 6:
        spot = -200 + 100 * (int(position)-4)
        goto(spot,0)
        if player == 0:
            eye()
        else:
            home()
    else:
        spot = -200 + 100 * (int(position)-7)
        goto(spot,-100)
        if player == 0:
            eye()
        else:
            home()


    
