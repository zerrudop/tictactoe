from turtle import *
from game_functions import * 
from functions import *

taken = []
player = 0
penup()
goto(-200,200)
pendown()
create_board()
while True:
    
    pos = textinput("Where to?", "Pick a square to go to (1-9)")
    if pos in taken:
        pos = textinput("Where to?", "That's taken!")
    
    if(int(pos) <= 9):
        make_move(player, pos)
        taken.append(pos)
        print(taken)
    else:
        break
    player = next_player(player)

done()