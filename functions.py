from turtle import *
speed(0)

#square board
def square(size, sq_color):
    begin_fill()
    fillcolor(sq_color)
    forward(size)
    right(90)
    forward(size)
    right(90)
    forward(size)
    right(90)
    forward(size)
    right(90)
    end_fill()

#white grid
def row():
    for i in range(3):
        pencolor("white")
        square(300/3, 'black')
        forward(100)
#starting point
penup()
goto(-200,200)
pendown()




def create_board(): 
    #theboard
    square(300, 'black')
    for i in range(3):

        row()
        penup()
        back(300)
        right(90)
        forward(300/3)
        left(90)
        pendown()
